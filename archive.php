<?php get_header(); ?>

	<main role="main">
		
		<section class="container clear">

			<h1 class="archive-title<?php if (get_the_archive_description()) : ?> with-description<?php endif; ?>"><?php single_cat_title(); ?></h1>
			
			<?php if (get_the_archive_description()) : ?>
				<?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
			<?php endif; ?>
			<div class="archive-content">
				<?php get_template_part('loop'); ?>
			</div>
			
			<?php get_sidebar(); ?>
			<div class="clear"></div>
		</section>
		
	</main>


<?php get_footer(); ?>
