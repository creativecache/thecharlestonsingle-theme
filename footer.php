			<footer class="footer clear" role="contentinfo">
				<div class="container">
					<section class="site-info">
						
					</section>
					
					<p class="copyright">
						&copy; <?php echo date('Y'); ?> <a href="<?php echo site_url(); ?>"><?php bloginfo('name'); ?></a>. All rights reserved. | designed, developed and hosted by <a href="https://creativecache.co/" target="_blank">Creative Cache</a>
					</p>
				</div>
			</footer>

		</div>

		<?php wp_footer(); ?>
	</body>
</html>
