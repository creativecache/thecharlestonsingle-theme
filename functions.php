<?php

function cc_adding_scripts() {
	wp_enqueue_style('style', get_template_directory_uri() . '/style.css', array(), rand(111,9999), all);
	wp_enqueue_script( 'customjs', get_template_directory_uri() . '/js/scripts.js', array('jquery'), rand(111,9999), true );
}
add_action( 'wp_enqueue_scripts', 'cc_adding_scripts' );

add_theme_support( 'post-thumbnails' );

function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
  register_nav_menu('legal-menu',__( 'Legal Menu' ));
}
add_action( 'init', 'register_my_menu' );