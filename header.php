<!doctype html>
<?php global $post ?>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php if (!is_home()): echo get_the_title($post->ID); ?> | <?php endif; bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="The Charleston Single">
		
		<?php wp_head(); ?>
		
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700|Playfair+Display:400,700" rel="stylesheet" lazyload>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" lazyload>
		
	</head>
	<body <?php body_class(); ?>>
		
		<header>
			
			<div class="container">
				
				<?php if (is_home()): ?>

				<h1 class="site-info">
					<a href="<?php echo site_url(); ?>"><span class="floated">the</span>Charleston Single</a>
				</h1>

				<?php else: ?>

				<div class="site-info">
					<a href="<?php echo site_url(); ?>"><span class="floated">the</span>Charleston Single</a>
				</div>

				<?php endif; ?>
				
				<div class="right-header">
					
					<a href="#menu" class="mobile-menu"><i class="fas fa-bars"></i> Menu</a>

					<nav id="main-nav" role="navigation" class="clear">

						<?php  if ( has_nav_menu( 'main-menu' ) ) {
							wp_nav_menu( array( 'theme_location' => 'main-menu' ) );
						} ?>

					</nav>
					
					<?php //get_template_part('searchform'); ?>
					
				</div>
			
			</div>
			</nav>
			
		</header>
