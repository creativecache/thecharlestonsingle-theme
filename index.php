<?php get_header(); ?>

	<main role="main"<?php if(is_home() || is_front_page()  ): ?> id="homepage"<?php endif; ?>>
		<section id="home-banner">
			<div class="home-content">
				<h2><?php echo bloginfo('description') ?></h2>
				<p>Vivamus non tellus eros. Mauris nec nulla in diam pharetra scelerisque quis vitae dui.</p>
				<a href="#" class="button">Join Now</a>
			</div>
		</section>
		<section id="level-display" class="clear">
			<div class="container">
				<div class="change-image">
					<img src="<?php echo get_template_directory_uri()  ?>/inc/images/logo_level-three.png" class="level-three-image level" />
					<img src="<?php echo get_template_directory_uri()  ?>/inc/images/logo_level-two.png" class="level-two-image level" />
					<img src="<?php echo get_template_directory_uri()  ?>/inc/images/logo_level-one.png" class="level-one-image level" />
					<img src="<?php echo get_template_directory_uri()  ?>/inc/images/logo_green.png" class="faded-image" />
					<img src="<?php echo get_template_directory_uri()  ?>/inc/images/logo_white.png" class="base-image" />
				</div>
				<div class="details">
					<ul>
						<li class="level-three">
							<button type="button">
								<h3>Porch</h3>
								<p>Vivamus non tellus eros. Mauris nec nulla in diam pharetra scelerisque quis vitae dui. Nam a mollis quam. Proin mollis quam at odio hendrerit faucibus. Nunc auctor pretium ex, ultricies consequat lectus faucibus ac.</p>
							</button>
						</li>
						<li class="level-two">
							<button type="button">
								<h3>Piazza</h3>
								<p>Vivamus non tellus eros. Mauris nec nulla in diam pharetra scelerisque quis vitae dui. Nam a mollis quam. Proin mollis quam at odio hendrerit faucibus. Nunc auctor pretium ex, ultricies consequat lectus faucibus ac.</p>
							</button>
						</li>
						<li class="level-one">
							<button type="button">
								<h3>Front Door</h3>
								<p>Vivamus non tellus eros. Mauris nec nulla in diam pharetra scelerisque quis vitae dui. Nam a mollis quam. Proin mollis quam at odio hendrerit faucibus. Nunc auctor pretium ex, ultricies consequat lectus faucibus ac.</p>
							</button>
						</li>
					</ul>
				</div>
			</div>
		</section>

	</main>

<?php get_footer(); ?>
