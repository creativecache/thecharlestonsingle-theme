jQuery(document).ready(function($) {
	
	function mobileFunctions() {
		if ($('.mobile-menu').is(':visible')) {
			
		} else {
			
		}
	}
	
	function scrollToTop($value) {
		$('html, body').animate({
			scrollTop: $($value).offset().top - 30
		 }, 750);
	}
	
	function levelDisplayHeight() {
		var detailheight = $('#level-display .details > ul').outerHeight();
	
		$('#level-display').css('min-height', detailheight);
	}
	
	$('a').each(function() {
		if ($(this).attr('target') == '_blank') {
			$(this).attr('rel', 'nofollow noreferrer');
		}	
	})
	
	$('a[href^="#"]').each(function() {
		$(this).click(function(e) {
			e.preventDefault();
			var href = $(this).attr('href');
			scrollToTop(href);
		});
	});
	
	$('#level-display button').on({
		'mouseenter focus': function() {
			var buttonClass = $(this).parent().prop('className');
			$('#level-display .base-image').css('visibility', 'hidden');
			$('#level-display .faded-image').show();
			$('#level-display .level').hide();
			$('#level-display .' + buttonClass + '-image').fadeIn(250);
		},
		'mouseleave focusout': function() {
			var buttonClass = $(this).parent().prop('className');
			$('#level-display .base-image').css('visibility', '');
			$('#level-display .faded-image').hide();
			$('#level-display .level').hide();
			$('#level-display .' + buttonClass + '-image').fadeOut(250);
		}
	});
	
	// Functions to call on page load
	levelDisplayHeight();

	//Functions to call when window is resized
	jQuery(window).resize(function($) {
		levelDisplayHeight();
	});

	//Functions to call when window is scrolled
	jQuery(window).scroll(function($) {
		
	});
	
});
